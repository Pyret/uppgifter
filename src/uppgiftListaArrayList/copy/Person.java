package uppgiftListaArrayList.copy;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Person {
	private String fnamn;
	private String enamn;
	private String email;
	private String mobilnr;
	
	
	public Person(String fnamn,String enamn, String email, String mobilnr){
		
		this.fnamn=fnamn;
		this.enamn=enamn;
		this.email=email;
		this.mobilnr=mobilnr;
	}
	
		

	public Person(){
		this.setFnamn("");
		this.setEnamn("");
		this.setEmail("");
		this.setMobilnr("");
	}
	
	public String getFnamn() {
		return fnamn;
	}

	public void setFnamn(String fnamn) {
		this.fnamn = fnamn;
	}

	public String getEnamn() {
		return enamn;
	}

	public void setEnamn(String enamn) {
		this.enamn = enamn;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobilnr() {
		return mobilnr;
	}

	public void setMobilnr(String mobilnr) {
		this.mobilnr = mobilnr;
	}

//	public String getPersonInfo(){
//		return fnamn +  "\t" + enamn + "\t" + email + "\t" + mobilnr;
//	}	
	
	}
	


