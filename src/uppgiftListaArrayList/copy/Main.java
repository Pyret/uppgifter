package uppgiftListaArrayList.copy;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
	static ArrayList <Person> personLista = new ArrayList<Person>();
	static Scanner scanner = new Scanner(System.in);
	
	static String fnamn;
	static String enamn;
	static String email;
	static String mobilnr;
	static String regexEmailPattern="^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	static String regexMobilnrPattern= "([-?\\d+(\\.\\d+\\s]{5,})?";

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println(Database.getPersonInfo(11).getFnamn());
		ArrayList<Person> people = Database.getAllPeopleInDatabase();
//		Person person1 = people.get(1);//.getFnamn();
		//person1.getEmail();
		
		
		
//		String mobilnr = "070070707";
//		System.out.println(" Validerar email? " + mobilnr.matches(regexMobilnrPattern));

//---------- Anropar en metod för att mata in användare --------			
		personInput();

		
		System.out.println("\nFörnamn\tEfternamn\tEmail\t\tMobilnr");
		System.out.println("------------------------------------------------");	


//---------- Anropar en metod som hämtar och skriver ut information på skärmen från databasen MySQL --------		
		Person person = Database.getPersonInfo(1);
		System.out.println(Database.getPersonInfo(1).getFnamn());
		
		
////---------- Anropar en metod som visar en lista på det som lagrats till databasen nu --------------------
//		personShowInfo();

		
		
//------------------- Olika tester -----------------------------------------------------------------------			
//		Person.setFnamn("Ann");	
//		personShowInfo();
		
//		Person person1 = new Person("Ann","Johansson","ann@smart.se","269654654");
//		person1.setFnamn("Åke");
//		System.out.println(person1.getFnamn());
//		
		
	}
	
	
//---------- Metod för att mata in användare --------	
	static void personInput(){
		
			for(int i=0;i<100;i++){
			System.out.print("\nDitt förnamn?\t");
			
					fnamn= scanner.next();
			
			if(fnamn.equalsIgnoreCase("stop"))break;
	
			
			System.out.print("\nDitt efternamn?\t");
			
				enamn= scanner.next();
			
			
				
			System.out.print("\nDin email?\t");
		
			do{
				email= scanner.next();
				
				if(email.equalsIgnoreCase("stop")) break;
				
				if(email.matches(regexEmailPattern)){
					break;
				}else{
					System.out.println("\nSkriv in din email korrekt, abc.defghijk@lmn.se ?\t");
				}
				
			}while(!email.matches(regexEmailPattern));
			
			
			
			System.out.print("\nDitt mobilnummer?\t");
			
			do{
				mobilnr= scanner.next();
				
				if(mobilnr.equalsIgnoreCase("stop")) break;
				
				if(mobilnr.matches(regexMobilnrPattern)){
					break;
				}else{
					System.out.println("\nSkriv in ditt mobilnr korrekt, endast siffror?\t");
				}
			
			}while(!mobilnr.matches(regexMobilnrPattern));
			
			
//------------Metod som lagrar til MySQL----------------------------			
			Database.setPersonInfo(fnamn,enamn,email,mobilnr);
			
			
//------------Skapar objekt i klassen Person-----------------------			
			personLista.add(new Person(fnamn,enamn,email,mobilnr));
			
			
			
		
			}
		 
		scanner.close();
		
	}
		
// Dessa användare har lagrats till databasen MySQL	
//	static void personShowInfo(){
//	for(Person person: personLista){
//		System.out.println(person.getPersonInfo()) ;
//	}
//	}


		
	}
	
