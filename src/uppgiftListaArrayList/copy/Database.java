package uppgiftListaArrayList.copy;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class Database {
	
	//------------Metod som lagrar information till MySQL----------------------------
		public static void setPersonInfo(String fnamn, String enamn, String email, String mobilnr){
			String url="jdbc:mysql://localhost/personer";
			String user="root";
			String password="";
			String table ="elever";
			
//			
			try{
	    	   Class.forName("com.mysql.jdbc.Driver").newInstance();
	    	   
	    	   // 1. Get a connection to database
	    	   Connection myConn = DriverManager.getConnection(url, user, password); 
	//    
	    	   
	    	   // 2. Create a statement
	    	   Statement myStmt= myConn.createStatement();
	    	   
	    	   // 3. Execute a SQL query
//	    	   String query="INSERT INTO elever (fnamn) VALUES ('Mikaela')";
//	  	   String query="INSERT INTO elever (fnamn,enamn,email,mobilnr) VALUES ('Jonas','Engelheart','engelheart@me.com','0705600000')";
	    	   String query="INSERT INTO elever (fnamn,enamn,email,mobilnr) VALUES ('" + fnamn + "','" + enamn + "','" + email + "','" + mobilnr + "');";
	   	   
	       myStmt.executeUpdate(query);

	       	  // 4. Process the result
	 //	   ResultSet myRs= myStmt.executeQuery("select * FROM " + table);
//	   	   while(myRs.next()){
//			   System.out.println(myRs.getString("id")+ "\t" + myRs.getString("fnamn")+ "\t" + myRs.getString("enamn")+ "\t"+ myRs.getString("email")+ "\t"+ myRs.getString("mobilnr")+ "\t");
//		   } 


	    	   
	       }
	       catch(Exception exc){
	    	   exc.printStackTrace();
	       }
			
			
			
		}
		
		
		public static ArrayList<Person> getAllPeopleInDatabase(){
			ArrayList <Person> people = new ArrayList<Person>();
			String url="jdbc:mysql://localhost/personer";
			String user="root";
			String password="";
			String table ="elever";
			Person person = null;
//			
			try{
	    	   Class.forName("com.mysql.jdbc.Driver").newInstance();
	    	   
	    	   // 1. Get a connection to database
	    	   Connection myConn = DriverManager.getConnection(url, user, password); 
	//    
	    	   
	    	   // 2. Create a statement
	    	   Statement myStmt= myConn.createStatement();
	    	   
	    	   // 3. Execute a SQL query
//	    	   String query="INSERT INTO elever (fnamn) VALUES ('Mikaela')";
//	  	   String query="INSERT INTO elever (fnamn,enamn,email,mobilnr) VALUES ('Jonas','Engelheart','engelheart@me.com',0705600000)";
//	    	   String query="INSERT INTO elever (fnamn,enamn,email,mobilnr) VALUES ('" + fnamn + "','" + enamn + "','" + email + "'," + mobilnr + ")";
	   	   
//	      myStmt.executeUpdate(query);

	       	  // 4. Process the result
	    	   ResultSet myRs= myStmt.executeQuery("select * FROM elever");
	       	   
	    	   while(myRs.next()){
	    		   myRs.getString("id");
	    		   myRs.getString("fnamn");
	    		   myRs.getString("enamn");
	    		   myRs.getString("email");
	    		   myRs.getString("mobilnr");
	    		   person = new Person(myRs.getString("fnamn"), myRs.getString("enamn"), myRs.getString("email"), myRs.getString("mobilnr") );
	    		   
	    		   //people.add(person);
	    		   //System.out.println(myRs.getString("id")+ "\t" + myRs.getString("fnamn")+ "\t" + myRs.getString("enamn")+ "\t"+ myRs.getString("email")+ "\t"+ myRs.getString("mobilnr"));
	    	   }
	   

	    	   
	       }
	       catch(Exception exc){
	    	   exc.printStackTrace();
	       }
			
			return people;
		}
	//------------Metod som hämtar informatil från MySQL----------------------------	
		public static Person getPersonInfo(int id){
			String url="jdbc:mysql://localhost/personer";
			String user="root";
			String password="";
			String table ="elever";
			Person person = null;
//			
			try{
	    	   Class.forName("com.mysql.jdbc.Driver").newInstance();
	    	   
	    	   // 1. Get a connection to database
	    	   Connection myConn = DriverManager.getConnection(url, user, password); 
	//    
	    	   
	    	   // 2. Create a statement
	    	   Statement myStmt= myConn.createStatement();
	    	   
	    	   // 3. Execute a SQL query
//	    	   String query="INSERT INTO elever (fnamn) VALUES ('Mikaela')";
//	  	   String query="INSERT INTO elever (fnamn,enamn,email,mobilnr) VALUES ('Jonas','Engelheart','engelheart@me.com',0705600000)";
//	    	   String query="INSERT INTO elever (fnamn,enamn,email,mobilnr) VALUES ('" + fnamn + "','" + enamn + "','" + email + "'," + mobilnr + ")";
	   	   
//	      myStmt.executeUpdate(query);

	       	  // 4. Process the result
	    	   ResultSet myRs= myStmt.executeQuery("select * FROM elever where id=" + id + ";");
	       	   
	    	   while(myRs.next()){
	    		   myRs.getString("id");
	    		   myRs.getString("fnamn");
	    		   myRs.getString("enamn");
	    		   myRs.getString("email");
	    		   myRs.getString("mobilnr");
	    		   person = new Person(myRs.getString("fnamn"), myRs.getString("enamn"), myRs.getString("email"), myRs.getString("mobilnr") );
	    		   
	    		   //people.add(person);
	    		   //System.out.println(myRs.getString("id")+ "\t" + myRs.getString("fnamn")+ "\t" + myRs.getString("enamn")+ "\t"+ myRs.getString("email")+ "\t"+ myRs.getString("mobilnr"));
	    	   }
	   

	    	   
	       }
	       catch(Exception exc){
	    	   exc.printStackTrace();
	       }
			return person; //new Person();
			
		}	
	

}
