import java.util.Scanner;


public class Fargatgang {
	static double bredd;
	static double hojd;
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	

double fargatgang=0,vaggyta = 0, dorryta=0, farg_m=0, burk_l=0;
int antal;
double variable1,variable2;
	Scanner user_input= new Scanner(System.in);
	
	//-------------- Väggyta --------------------------
	System.out.println("\nHur många väggar finns det i rummet?");
		antal=user_input.nextInt();
		
		for(int i=1;i<=antal;i++){
			System.out.println("\nVilken bredd har vägg "+i+": ");
				bredd = user_input.nextDouble();
			System.out.println("\nVilken höjd har vägg "+i+": ");
				hojd = user_input.nextDouble();
			
			vaggyta=vaggyta+getArea(bredd,hojd);
		}
	
	//-------------- Dörryta --------------------------
		System.out.println("\nHur många dörrar finns det i rummet?");
			antal=user_input.nextInt();
			
			for(int i=1;i<=antal;i++){
				System.out.println("\nVilken bredd har dörr "+i+": ");
					bredd = user_input.nextDouble();
				System.out.println("\nVilken höjd har dörr "+i+": ");
					hojd = user_input.nextDouble();
				
				dorryta=dorryta+getArea(bredd,hojd);
			}
			
	//-------------- Resultaten --------------------------	
			System.out.println("\nTotala väggytan är "+ vaggyta + " m²");
			System.out.println("Totala dörrytan är "+ dorryta + " m²");
			System.out.println("Totala ytan som ska målas är " + getPaintArea(vaggyta,dorryta)+ " m²");
	
	//-------------- Fråga och resultat, m²/liter färg --------------------------
			System.out.println("\nHur många m² räcker en liter färg till?");
			farg_m=user_input.nextInt();		
			System.out.println("En liter färg räcker till "+farg_m+ " m²");
			System.out.println("Det går åt ca "+ fargatgang(vaggyta,dorryta,farg_m) + " liter");
			
	//-------------- Fråga och resultat, liter färg/burk --------------------------
			System.out.println("\nHur stor är färgburken i liter?");
			burk_l=user_input.nextInt();					
		

//	//-------------- Avrundar uppåt till heltal FUNGERAR EJ--------------------------	
			
			variable1=burk(vaggyta,dorryta,burk_l,farg_m)-Math.round(burk(vaggyta,dorryta,burk_l,farg_m));
			if (variable1>0){
				variable1=Math.round(burk(vaggyta,dorryta,burk_l,farg_m))+1;
			}
			else{
				variable1=Math.round(burk(vaggyta,dorryta,burk_l,farg_m));	
			}	
		System.out.println("Det går åt "+ variable1 + " st "+ burk_l+" liters burkar");
		

			if (fargatgang(vaggyta,dorryta,farg_m)%burk_l>0){
				variable2=Math.round(fargatgang(vaggyta,dorryta,farg_m)/burk_l)+1;	
			}
			else{
				variable2=Math.round(fargatgang(vaggyta,dorryta,farg_m)/burk_l);	
			}
			System.out.println("Det går åt "+ variable1 + " st "+ burk_l+" liters burkar");
			System.out.println("Det går åt "+ variable2 + " st "+ burk_l+" liters burkar");
}

public static double getArea(double bredd,double hojd){
	return bredd*hojd;
}
public static double getPaintArea(double vaggyta,double dorryta){
	return (vaggyta-dorryta);
}

public static double fargatgang(double vaggyta,double dorryta,double farg_m){
	return (vaggyta-dorryta)/farg_m;
}
public static double burk(double vaggyta,double dorryta,double burk_l, double farg_m){
	return fargatgang(vaggyta,dorryta,farg_m)/burk_l;
}
}