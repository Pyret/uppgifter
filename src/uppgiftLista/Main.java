package uppgiftLista;

import java.util.Scanner;

public class Main {
	static Person personArray[]= new Person[2];
	static Scanner scanner = new Scanner(System.in);
	
	static String fnamn;
	static String enamn;
	static String email;
	static String mobilnr;
	public static void main(String[] args) {
		// TODO Auto-generated method stub


		personInput();

		System.out.println("\nFörnamn\tEfternamn\tEmail\t\t\tMobilnr");
		System.out.println("------------------------------------------------");	
		personShowInfo();

		
		
			
//		Person.setFnamn("Ann");	
//		personShowInfo();
		
//		Person person1 = new Person("Ann","Johansson","ann@smart.se","269654654");
//		person1.setFnamn("Åke");
//		System.out.println(person1.getFnamn());
//		
		
	}
	static void personInput(){
		for(int i=0;i<2;i++){
		
		System.out.print("\nDitt förnamn?\t");
		fnamn= scanner.next();
		
		System.out.print("\nDitt efternamn?\t");
		enamn= scanner.next();
		
		System.out.print("\nDin email?\t");
		email= scanner.next();

		
		System.out.print("\nDitt mobilnummer?\t");
		mobilnr= scanner.next();

		personArray[i]=new Person(fnamn,enamn,email,mobilnr);	
		}
		scanner.close();
	}
	
	static void personShowInfo(){
	for(Person person: personArray){
		System.out.println(person.getPersonInfo()) ;
	}
	}
}
