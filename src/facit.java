

	import java.util.Arrays;
public class facit {



		public static void main(String[] args) {
			// Uppgift 1
			System.out.println("Is even: " + isEven(10));

			// Uppgift 2
			printCalculations(2, 5);

			// Uppgift 3
			System.out.println(andify("Först", "Sedan"));
			System.out.println(andify("Tredje", "Fjärde", "Femte"));
			System.out.println(andify("Sjätte", "Sjunde", "Åttonde", "Nionde"));

			// Uppgift 4
			System.out.println(Arrays.deepToString(asciiValues(new String[] {
					"abc", "def" })));

//			// Uppgift 4a
//			LetterArray letterArray = asciiValuesLetterArray(new String[] { "abc",
//					"def" });
//			letterArray.printAsciiValues();

			// Uppgift 5
			System.out.println(getNextTrainTime(13.25));

		}

		public static boolean isEven(int tal) {
			return tal % 2 == 0;
		}

		public static void printCalculations(int first, int second) {
			System.out.println(first + " + " + second + " = " + (first + second));
			System.out.println(first + " - " + second + " = " + (first - second));
			System.out.println(first + " / " + second + " = " + (first / second));
			System.out.println(first + " * " + second + " = " + (first * second));
		}

		public static String andify(String a, String b) {
			return a + " och " + b;
		}

		public static String andify(String a, String b, String c) {
			return a + " och " + b + " och " + c;
		}

		public static String andify(String a, String b, String c, String d) {
			return a + " och " + b + " och " + c + " och " + d;
		}

		public static int[][] asciiValues(String[] strings) {
			int letterCount[][] = new int[strings.length][];

			// Iterera över alla strängar
			for (int i = 0; i < strings.length; i++) {
				String str = strings[i];
				letterCount[i] = new int[str.length()];

				// Loopa över strängen med index "i" och konvertera bokstäverna till
				// ascii-vörden
				for (int j = 0; j < str.length(); j++) {
					letterCount[i][j] = str.charAt(j);
				}
			}

			return letterCount;
		}

//		public static LetterArray asciiValuesLetterArray(String[] strings) {
//		//	 Använd den redan skrivna asciiValues()
//			return new LetterArray(strings, asciiValues(strings));
//		}

		public static double getNextTrainTime(double time) {
			// Timmar är heltalsdelen
			int hours = (int) time;

			// Minuter är fraktalen. 12.25 => 0.25. Konvertera sedan till ett
			// vettigt heltal
			int minutes = (int) (100 * (time - hours));

			if (minutes >= 45) {
				// 12.55 => 1 + 12 + 0.15 = 13.15
				return 1 + hours + 0.15;
			} else {
				// 12.25 => 12 + 0.45 = 12.45
				return hours + 0.45;
			}
		}
	
		public class LetterArray {

			private String[] strings;
			private int[][] asciiValues;

			public LetterArray(String[] strings, int[][] asciiValues) {
				this.strings = strings;
				this.asciiValues = asciiValues;
			}

			public void printAsciiValues() {
				for (int i = 0; i < this.asciiValues.length; i++) {
					System.out.println(this.strings[i] + ":");
					for (int j = 0; j < this.asciiValues[i].length; j++) {
						System.out.println(j + ": " + this.asciiValues[i][j]);
					}
					System.out.println();
				}
			}
		}


}


