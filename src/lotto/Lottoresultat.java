package lotto;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;


public class Lottoresultat {

	public static void main(String[] args) throws IOException{
	
		Random rand = new Random();
		int[] slump = new int[11]; 	//slumptal array 
		int[] lottoNr= new int[7]; 	//sorterar lottonummer
		int[] tillaggNr= new int[4];//sorterar tilläggsnummer
		int[] arrayNr= new int[7]; 	//l�ser in lottoraden fr�n inl�st fil
		int lotto=0;				//antal r�tt p� lotto	
		int tillagg=0;				//antal r�tt p� till�gg	
		int tmp = 0;				//tempor�rt slumptal 
		boolean unik;
		
		File input = new File("C:\\Users\\Pyret\\Dropbox\\Java\\Uppgifter\\lottorad.txt");
		Scanner lasFil = new Scanner(input);
		
// Lottorad h�mtad fr�n fil	
		System.out.println("Du har valt lottorad, i nummerordning:");
		for(int i=0; i< arrayNr.length; i++){
			arrayNr[i]=lasFil.nextInt();
		}
//Sorterar och skriver ut lottoraden i nummer ordning
		Arrays.sort(arrayNr);
		for(int i=0; i< arrayNr.length; i++){
			System.out.print(arrayNr[i] + " ");
		}
	
//Skapar och loopar slumptal och kollar s� att dom �r unika	
		for (int i = 0; i < slump.length; i++) {

			unik = false;
			while (!unik) {
				tmp = rand.nextInt(35) +1;
				unik = true;
				for (int j = 0; j < slump.length; j++) {
					if (tmp == slump[j]) {
						unik = false;
					}
				}
			}
			slump[i] = tmp;
		}

//Skriver osorterade lottoraden		
		System.out.println("\n\nVeckans rätta lottorad, osorterad:");		
		for (int i = 0; i < 7; i++) {
		System.out.print(slump[i] +" ");		
		}
		System.out.print("\nTilläggsnummer: ");
		for (int i = 7; i < 11; i++) {
			System.out.print(slump[i] +" ");		
			}
		
//Sparar lottonummer		
		for (int i = 0; i < 7; i++) {
			lottoNr[i]=slump[i];
		}

//Spara tilläggsnummer		
		tillaggNr[0]=slump[7];
		tillaggNr[1]=slump[8];
		tillaggNr[2]=slump[9];
		tillaggNr[3]=slump[10];
		
		
//Skriver ut den r�tta slumpade lottoraden samt tilläggsnummer		
		System.out.println("\n\nVeckans rätta lottorad, sorterad:");		

//Sorterar lottonummer och skriver ut		
		Arrays.sort(lottoNr);
		for (int i = 0; i < 7; i++) {
		System.out.print(lottoNr[i] +" ");
		}
		System.out.println(" ");
		System.out.print("Tilläggsnummer: ");

//Sorterar och skriver ut tilläggsnummer
		Arrays.sort(tillaggNr);
		for (int i = 0; i < 4; i++) {
			System.out.print(tillaggNr[i] +" ");
			}
			

		//Kontrollerar resultatet mot din lottoraden, slump[0-6]
		for (int i = 0; i < arrayNr.length; i++) {
			if(slump[0]==arrayNr[i]){
				lotto++;		//beräknar antalet rätt på lotto
			}
			if(slump[1]==arrayNr[i]){
				lotto++;
			}
			if(slump[2]==arrayNr[i]){
				lotto++;
			}
			if(slump[3]==arrayNr[i]){
				lotto++;
			}
			if(slump[4]==arrayNr[i]){
				lotto++;
			}
			if(slump[5]==arrayNr[i]){
				lotto++;
			}
			if(slump[6]==arrayNr[i]){
				lotto++;
			}
			
//Forts�tter att kontrollera till�ggsnummer mot din lottorad, slump[7-10]
			if(slump[7]==arrayNr[i]){
				tillagg++;		//ber�knar antalet r�tt p� till�gsnummer
			}
			if(slump[8]==arrayNr[i]){
				tillagg++;
			}
			if(slump[9]==arrayNr[i]){
				tillagg++;
			}
			if(slump[10]==arrayNr[i]){
				tillagg++;
			}
		}	

			
			System.out.println("\n\nDitt resultat är:");
			System.out.println(lotto +" + "+tillagg+ " rätt");
			}
			
		}