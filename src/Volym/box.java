package Volym;

public class box {
	double width;
	double height;
	double deep;
	
	public box(double w){
		width=w;
		height=10;
		deep=10;
	}
	
	public box(double h,double d){
		width=10;
		height=h;
		deep=d;
	}
	
	public box(double w,double h, double d){
		width=w;
		height=h;
		deep=d;
	}

	double getWidth(){
		return width;
	}
	
	double getHeight(){
		return height;
	}

	double getDeep(){
		return deep;
	}
	
	Double calcVolym(){
		return getWidth() * getHeight() * getDeep();
	}
}