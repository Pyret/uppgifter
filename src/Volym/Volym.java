package Volym;

import java.util.Scanner;


public class Volym {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Deklarerar variabler
		double width=0;
		double height=0;
		double deep=0;
		String colour="white";
		
		Scanner user_input= new Scanner(System.in);
		
		System.out.println("\n------Färglös box--------");
		System.out.print("\nSkriv en bredd?\t");
		width=user_input.nextDouble();
		System.out.print("\nSkriv en höjd?\t");
		height=user_input.nextDouble();
		System.out.print("\nSkriv en djup?\t"); 
		deep=user_input.nextDouble();
		//Skickar in värden till klassen Box
		box volym= new box(width,height,deep);
		
		System.out.println("\n-----------------------------");
		System.out.println("Bredd \t: " + volym.getWidth());
		System.out.println("Höjd  \t: " + volym.getHeight());
		System.out.println("Djup  \t: " + volym.getDeep());
		System.out.println("Volym \t: " + volym.calcVolym());
		System.out.println("-----------------------------");
		
		System.out.println("\n------Färgad box--------");
		System.out.print("\nSkriv en bredd?\t");
		width=user_input.nextDouble();
		System.out.print("\nSkriv en höjd?\t");
		height=user_input.nextDouble();
		System.out.print("\nSkriv en djup?\t"); 
		deep=user_input.nextDouble();
		System.out.print("\nVilken färg?\t"); 
		colour=user_input.next();
		
		//Skickar in värden till klassen Box
		boxColour farg = new boxColour(width, height, deep, colour);
		
		System.out.println("\n-----------------------------");
		System.out.println("Bredd \t: " + farg.getWidth());
		System.out.println("Höjd  \t: " + farg.getHeight());
		System.out.println("Djup  \t: " + farg.getDeep());
		System.out.println("Volym \t: " + farg.calcVolym());
		
		farg.printColour();
		System.out.println("-----------------------------");
		
		//------- Skapar ett objekt i Class box
//		box volym= new box(width,height,deep);
//		box volym= new box(height=5,deep=10);
		
	 	
		
	}

	

	
}
